import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppSerService {
  @Output() test = new EventEmitter<boolean>();

  constructor() 
  { 
    this.gul();
  }

  gul() {
    let url = 'https://jsonplaceholder.typicode.com/posts';
    fetch(url).then((wis) => {
      console.log('luka')
      this.setDane(true);
    });
  }

  setDane(flag: boolean) {
    this.test.emit(flag);
  }

  getDane() {
    return this.test;
  }

}
