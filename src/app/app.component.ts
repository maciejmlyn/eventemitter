import { Component, OnInit } from '@angular/core';
import { AppSerService } from './app-ser.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angels';

  constructor(private appSerService: AppSerService) 
  { 
    this.appSerService.getDane().subscribe((data) => {
      console.log('componentLog');
    })
    
  }

  ngOnInit() {
  }
}
